/*

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.

*/

package main

// Internal Go modules
import (
	"encoding/csv"
	"fmt"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"
)

// External Go modules
import (
	"github.com/bwmarrin/discordgo"
	scribble "github.com/nanobox-io/golang-scribble"
	"strconv"
	"time"
)

var db, _ = scribble.New("./", nil)

// Config The config.json struct
type Config struct {
	Token string
}

// GuildSettings Guild-specific config
type GuildSettings struct {
	ChannelID string
	Starcount int
	Prefix    string
	Emoji     string
}

func initialize() Config {
	var config Config

	var err = db.Read("./", "config", &config)
	if err != nil {
		config.Token = "Token here"
		db.Write("./", "config", config)
		println("Please check ./config.json for Settings before starting the bot.")
		os.Exit(1)
	}

	return config
}

func main() {
	var config = initialize()

	discord, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		panic(err)
	}

	discord.AddHandler(messageCreate)
	discord.AddHandler(messageUpdate)
	discord.AddHandler(messageReactionAdd)
	discord.AddHandler(messageReactionRemove)
	discord.Open()

	println("Bot is ready.")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	discord.Close()
}

func messageCreate(session *discordgo.Session, message *discordgo.MessageCreate) {
	var settings, err = ReadDB(session, message.ChannelID)
	if err != nil {
		session.ChannelMessageSend(message.ChannelID, fmt.Sprintln(err))
		return
	}

	var prefix = settings.Prefix

	if !strings.HasPrefix(message.Content, prefix) {
		return
	}

	var setPrefixCommand = prefix + "setPrefix"
	if strings.HasPrefix(message.Content, setPrefixCommand) {
		input := csv.NewReader(strings.NewReader(message.Content))
		input.Comma = ' ' // delimiter

		args, err := input.Read()
		if err != nil || len(args) != 2 {
			session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Invalid argument! Usage: `%s [prefix]`", setPrefixCommand))
			return
		}

		settings.Prefix = args[1]

		WritetoDB(session, message, settings)
	}

	var setChannelCommand = prefix + "setChannel"
	if strings.HasPrefix(message.Content, setChannelCommand) {
		input := csv.NewReader(strings.NewReader(message.Content))
		input.Comma = ' ' // delimiter

		args, err := input.Read()
		if err != nil || len(args) != 2 {
			session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Invalid argument! Usage: `%s` [Channel *Mention]", setChannelCommand))
			return
		}

		var regex = regexp.MustCompile("[0-9]*")
		var channel = regex.FindAllString(args[1], 3)

		//                         v No idea why it's 2, I hate regex
		settings.ChannelID = channel[2]

		WritetoDB(session, message, settings)
	}

	var setStarCommand = prefix + "setStar"
	if strings.HasPrefix(message.Content, setStarCommand) {
		input := csv.NewReader(strings.NewReader(message.Content))
		input.Comma = ' ' // delimiter

		args, err := input.Read()
		if err != nil || len(args) != 2 {
			session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Invalid argument! Usage: `%s [prefix]`", setStarCommand))
			return
		}

		var starcount, parseErr = strconv.Atoi(args[1])
		if parseErr != nil {
			session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Invalid argument! Usage: `%s [int]`", setStarCommand))
			return
		}

		settings.Starcount = starcount

		WritetoDB(session, message, settings)
	}

	var setEmojiCommand = prefix + "setEmoji"
	if strings.HasPrefix(message.Content, setEmojiCommand) {
		input := csv.NewReader(strings.NewReader(message.Content))
		input.Comma = ' ' // delimiter

		args, err := input.Read()
		if err != nil || len(args) != 2 {
			session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Invalid argument! Usage: `%s [emoji]`", setEmojiCommand))
			return
		}

		settings.Emoji = args[1]

		WritetoDB(session, message, settings)
	}
}

// embedContent The content of an embed, made into a struct for easier organizing
type embedContent struct {
	Title     string
	Content   string
	Username  string
	Avatar    string
	Timestamp string
}

func messageUpdate(session *discordgo.Session, message *discordgo.MessageUpdate) {
	var settings, err = ReadDB(session, message.ChannelID)
	if err != nil {
		session.ChannelMessageSend(message.ChannelID, fmt.Sprintln(err))
		return
	}

	if len(message.Message.Reactions) != 0 {
		for _, reaction := range message.Message.Reactions {
			fmt.Println(reaction.Emoji.ID)
			if reaction.Emoji.ID >= settings.Emoji {
				var reactCount = reaction.Count

				if reaction.Me == true {
					reactCount = reactCount - 1
				}

				if reactCount >= settings.Starcount {
					thatMessage, err := searchStarboard(session, message.ID, settings)
					if err != nil {
						session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Couldn't get messages! Error: ```%s```", err))
						return
					}

					var embed = thatMessage.Embeds[0]
					embed.Description = message.Content

					session.ChannelMessageEditEmbed(settings.ChannelID, thatMessage.ID, embed)
				}
			} else {
				continue
			}
		}
	}
}

func messageReactionAdd(session *discordgo.Session, message *discordgo.MessageReactionAdd) {
	//fmt.Printf("%v", message.MessageReaction)
	println("LA")
	checkReactions(session, message.MessageReaction)
}

func messageReactionRemove(session *discordgo.Session, message *discordgo.MessageReactionRemove) {
	//fmt.Printf("%v", message.MessageReaction)
	println("LR")
	checkReactions(session, message.MessageReaction)
}

func checkReactions(session *discordgo.Session, message *discordgo.MessageReaction) {
	var settings, err = ReadDB(session, message.ChannelID)
	if err != nil {
		session.ChannelMessageSend(message.ChannelID, fmt.Sprintln(err))
		return
	}

	if settings.ChannelID == "" {
		session.ChannelMessageSend(message.ChannelID, "Channel ID is not set! Check `help`")
	} else if settings.ChannelID == message.ChannelID {
		return
	}

	users, err := session.MessageReactions(message.ChannelID, message.MessageID, settings.Emoji, -1)
	if err != nil {
		session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Couldn't get reactions! Error: ```%s```", err))
		return
	}

	starredMessage, err1 := session.ChannelMessage(message.ChannelID, message.MessageID)
	if err1 != nil {
		session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Couldn't get the targeted message! Error: ```%s```", err))
		return
	}

	starcount := len(users)

	if starredMessage.Author.ID == message.UserID {
		starcount = starcount - 1
	}

	if starcount < settings.Starcount {
		thatMessage, err := searchStarboard(session, message.MessageID, settings)
		if err == nil {
			err2 := session.ChannelMessageDelete(settings.ChannelID, thatMessage.ID)
			if err2 != nil {
				return
			}
		} else {
			return
		}
	} else if starcount >= settings.Starcount {
		thatMessage, err := searchStarboard(session, message.MessageID, settings)
		if err != nil {
			targetedGuild, err1 := session.Channel(message.ChannelID)
			targetedUser, err2 := session.GuildMember(targetedGuild.GuildID, message.UserID)
			if err1 != nil || err2 != nil {
				var err = fmt.Errorf("%s\n%s", err1, err2)
				session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Couldn't get message! Error: ```%s```", err))
				return
			}

			var timestamp, timestampErr = starredMessage.Timestamp.Parse()
			if timestampErr != nil {
				session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Couldn't convert timestamp! Error: ```%s```", err))
				return
			}
			var timestampString = timestamp.Format(time.ANSIC)

			var messageContent embedContent

			if targetedUser.Nick == "" {
				messageContent.Username = starredMessage.Author.Username + "#" + starredMessage.Author.Discriminator
			} else {
				messageContent.Username = targetedUser.Nick + " (" + starredMessage.Author.Username + "#" + starredMessage.Author.Discriminator + ")"
			}

			messageContent.Title = fmt.Sprintf("%d⭐ ", settings.Starcount)
			messageContent.Content = starredMessage.Content
			messageContent.Avatar = starredMessage.Author.AvatarURL("64")
			messageContent.Timestamp = timestampString

			var send = new(discordgo.MessageSend)
			send.Content = messageContent.Title

			var embed = new(discordgo.MessageEmbed)
			embed.Description = messageContent.Content
			embed.Footer = &discordgo.MessageEmbedFooter{
				Text: message.MessageID + " / " + messageContent.Timestamp,
			}
			embed.Author = &discordgo.MessageEmbedAuthor{
				Name:    messageContent.Username,
				IconURL: messageContent.Avatar,
			}

			send.Embed = embed

			//        v Excuse this, `var` statements are not allowed in an `if` condition
			if _, err := session.ChannelMessageSendComplex(settings.ChannelID, send); err != nil {
				session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Can't send to targeted channel <#%v>! Error: ```\n%v```", settings.ChannelID, err))
				return
			}
		} else {
			var edit = new(discordgo.MessageEdit)
			*edit.Content = fmt.Sprintf("%d⭐ ", settings.Starcount)
			edit.Embed = thatMessage.Embeds[0]
			session.ChannelMessageEditComplex(edit)
		}
	} else {

	}
}

func searchStarboard(session *discordgo.Session, OriginalID string, settings GuildSettings) (*discordgo.Message, error) {
	var allMessages, err = session.ChannelMessages(settings.ChannelID, 10, "", "", "")
	if err != nil {
		return nil, fmt.Errorf("Couldn't get messages! Error: ```%s```", err)
	}

	for _, individual := range allMessages {
		if len(individual.Embeds) == 0 {
			continue
		}

		if footer := individual.Embeds[0].Footer.Text; strings.Split(footer, " / ")[0] == OriginalID {
			return individual, nil
		}
	}

	return nil, fmt.Errorf("Can't find any messages! ")
}

// WritetoDB Function to write Config to Database
func WritetoDB(session *discordgo.Session, message *discordgo.MessageCreate, settings GuildSettings) {
	var Channel, err = session.Channel(message.ChannelID)
	if err != nil {
		return
	}

	dberr := db.Write("Guilds", Channel.GuildID, settings)
	if dberr != nil {
		session.ChannelMessageSend(message.ChannelID, "Can't write to config! Error: ```"+fmt.Sprintf("%v", dberr)+"```")
		return
	}

	//fmt.Printf("%v", config)

	session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Finished writing to config channel <#%s>", message.ChannelID))
	return
}

// ReadDB Function to read guild-specific Config from Database
func ReadDB(session *discordgo.Session, ChannelID string) (GuildSettings, error) {
	var settings GuildSettings

	var Channel, err = session.Channel(ChannelID)
	if err != nil {
		return settings, err
	}

	dberr := db.Read("Guilds", Channel.GuildID, &settings)
	if dberr != nil {
		settings.ChannelID = ""
		settings.Emoji = "⭐"
		settings.Prefix = "./"
		settings.Starcount = 3
		writeErr := db.Write("Guilds", Channel.GuildID, &settings)
		if writeErr != nil {
			return settings, fmt.Errorf("Can't read from config! Error: ```" + fmt.Sprintf("%v", dberr) + "```")
		}
	}

	return settings, nil
}
